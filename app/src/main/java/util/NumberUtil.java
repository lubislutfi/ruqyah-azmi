package util;

import android.content.Context;

/**
 * Created by kopral on 10/4/16.
 */

public class NumberUtil {

    Context cont;

    public NumberUtil(Context val_cont) {
        this.cont = val_cont;
    }

    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String zeroLeftPadding(int val_length, int val_padValue) {
        return String.format("%0" + String.valueOf(val_length) + "d", val_padValue);
    }

    public String zeroLeftPadding_Remove(String value) {
        return value.replaceFirst("^0+(?!$)", "");
    }

}
