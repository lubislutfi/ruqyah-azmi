package util;

import android.content.Context;
import android.util.Log;

import com.bit.ruqyah.R;
import org.json.JSONException;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import CONST.FILE_CONST;
import CONST.PARAMS_CONST;

/**
 * Created by kopral on 10/9/16.
 */

public class FileUtility {

    Context cont;
    NumberUtil nu;

    public FileUtility(Context val_cont) {
        this.cont = val_cont;
        nu = new NumberUtil(this.cont);
    }

    //ambil path root
    private String getRootPath() {
        String path = cont.getExternalFilesDir(null).getPath();
        return path;
    }

    //ambil path media
    public String getMediaPath() {
        String path = getRootPath() + "/" + ".media";
        return path;
    }

    //ambil path qori
    private String getQoriPath() {
        String path = getMediaPath() + "/" + "qori";
        return path;
    }

    //ambil list qori
    public ArrayList<HashMap<String, String>> getListQori() {
        ArrayList<HashMap<String, String>> arlist = new ArrayList<HashMap<String, String>>();

        File qoriFolder = new File(getQoriPath());
        File qoriFolderList[] = qoriFolder.listFiles();

       /* if (qoriFolderList != null && qoriFolderList.length > 1) {
            Arrays.sort(qoriFolderList, new Comparator<File>() {
                @Override
                public int compare(File object1, File object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }*/
        int qoriFolderListLength = (qoriFolderList != null) ? qoriFolderList.length : 0;

        for (int i = 0; i < qoriFolderListLength; i++) {
            File surahFile = new File(getQoriPath() + "/" + qoriFolderList[i].getName());
            File surahFileList[] = surahFile.listFiles();


            /*if (surahFileList != null && surahFileList.length > 1) {
                Arrays.sort(surahFileList, new Comparator<File>() {
                    @Override
                    public int compare(File object1, File object2) {
                        return object1.getName().compareTo(object2.getName());
                    }
                });
            }*/
            String[] noSurah = new String[surahFileList.length];

            try {
                for (int j = 0; j < surahFileList.length; j++) {
                    noSurah[j] = nu.zeroLeftPadding_Remove(surahFileList[j].getName().substring(0, 3));
                }

                String[] jmlSurah = new HashSet<String>(Arrays.asList(noSurah)).toArray(new String[0]);

                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", String.valueOf(qoriFolderList[i].getName()));
                map.put("title", FILE_CONST.QORI_LIST().getJSONObject(Integer.parseInt(qoriFolderList[i].getName())).get("name").toString());
                map.put("subtitle", jmlSurah.length + " Surat");
                arlist.add(map);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.d("QoriList", arlist.toString());
        return arlist;
    }

    //ambil list qori
    public ArrayList<HashMap<String, String>> getListQoriFiles(String val_qori) {
        ArrayList<HashMap<String, String>> arlist = new ArrayList<HashMap<String, String>>();
        String[] namaSurat = cont.getResources().getStringArray(R.array.nama_surat);

        File surahFile = new File(getQoriPath() + "/" + val_qori);
        File surahFileList[] = surahFile.listFiles();

        //sort by name
        /*if (surahFileList != null && surahFileList.length > 1) {
            Arrays.sort(surahFileList, new Comparator<File>() {
                @Override
                public int compare(File object1, File object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }*/

        String[] surahAllFIles = new String[surahFileList.length];

        int surahFileListLength = (surahFileList != null) ? surahFileList.length : 0;

        for (int j = 0; j < surahFileListLength; j++) {
            surahAllFIles[j] = surahFileList[j].getName().substring(0, 3);
            //Log.d("SurahList", surahFileList[j].getName());
        }

        final String[] surahDistinct = new HashSet<String>(Arrays.asList(surahAllFIles)).toArray(new String[0]);
        //sort by name
        Arrays.sort(surahDistinct);

        for (int k = 0; k < surahDistinct.length; k++) {

            //jumlah ayat
            final int index = k;
            String[] ayahFiles = surahFile.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.startsWith(surahDistinct[index]) && name.endsWith(".mp3")) {
                        return true;
                    }
                    return false;
                }
            });

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", String.valueOf(surahDistinct[k]));
            map.put("title", "QS. " + nu.zeroLeftPadding_Remove(surahDistinct[k]) + ": " + namaSurat[Integer.parseInt(nu.zeroLeftPadding_Remove(surahDistinct[k])) - 1]);
            map.put("subtitle", ayahFiles.length + " Ayat");

            arlist.add(map);
        }
        Log.d("SurahList", arlist.toString());
        return arlist;
    }

    public String getParamQoriPath() {
        String path = getQoriPath() + "/" + PARAMS_CONST.QORI;
        Log.d("PARAM QORI", "GETFILE |" + "QORI: " + PARAMS_CONST.QORI + ", path: " + path);
        return path;
    }

    public String getQoriUrl() {
        try {
            String url = FILE_CONST.QORI_LIST().getJSONObject(Integer.parseInt(PARAMS_CONST.QORI)).getString("url");
            Log.d("PARAM QORI", "GETFILE |" + "QORI: " + PARAMS_CONST.QORI + ", url: " + url);
            return url;
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getAudioFileLocalPath(int val_surat, int val_ayat) {
        String fileName = nu.zeroLeftPadding(3, val_surat) + "" + nu.zeroLeftPadding(3, val_ayat) + ".mp3";
        String path = getParamQoriPath() + "/" + fileName;
        return path;
    }

    public String getAudioFileUrl(int val_surat, int val_ayat) {
        String fileName = nu.zeroLeftPadding(3, val_surat) + "" + nu.zeroLeftPadding(3, val_ayat) + ".mp3";
        String path = getQoriUrl() + "/" + fileName;
        return path;
    }

    //cek 'n buat direktori aplikasi kalau gak ada.
    public boolean checkBasicPath() {
        //root path
        File folder = new File(getRootPath());
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            success = true;
        } else {
            return false;
        }

        //media path
        folder = new File(getMediaPath());
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            success = true;
        } else {
            return false;
        }

        //qori path
        folder = new File(getQoriPath());
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            return true;
        } else {
            return false;
        }
    }

    //cek direktori qori yang sedang aktif(berdasarkan settingan)
    public boolean checkCurrentQoriPath() {
        if (checkBasicPath()) {
            File folder = new File(getParamQoriPath());
            boolean success = true;
            if (!folder.exists()) {
                success = folder.mkdir();
            }
            if (success) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isAyatFileExist(String val_fileName) {
        File file = new File(val_fileName);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteFile(String val_qori, final String val_surat) {
        boolean success = false;

        File qoriFolder = new File(getQoriPath() + "/" + val_qori);

        if (val_surat.equalsIgnoreCase("0")) {
            if (qoriFolder.isDirectory()) {
                String[] children = qoriFolder.list();
                for (int i = 0; i < children.length; i++) {
                    File file = new File(qoriFolder, children[i]);
                    if (file.delete()) {
                        success = true;
                    } else {
                        return false;
                    }
                }

                if (qoriFolder.exists()) {
                    if (qoriFolder.delete()) {
                        success = true;
                    } else {
                        return false;
                    }
                }
            }
        } else {
            String[] surahFiles = qoriFolder.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.startsWith(val_surat) && name.endsWith(".mp3")) {
                        return true;
                    }
                    return false;
                }
            });
            Log.d("HAPUS", "Multiple Delete LIST: " + surahFiles);

            for (int i = 0; i < surahFiles.length; i++) {
                File fileSurah = new File(getQoriPath() + "/" + val_qori + "/" + surahFiles[i]);
                if (fileSurah.exists()) {
                    success = fileSurah.delete();
                    Log.d("HAPUS", "Multiple Delete: " + success);
                } else {
                    return false;
                }
            }

            File qoriFolderToCheck = new File(getQoriPath() + "/" + val_qori);
            if (qoriFolderToCheck.listFiles().length == 0) {
                if (qoriFolderToCheck.delete()) {
                    success = true;
                } else {
                    return false;
                }
            }
        }

        return success;
    }
}
