package CONST;

/**
 * Created by kopral on 10/9/16.
 */

public class PLAY_CONST {
    public static boolean IS_AUDIOQURAN_PAUSED;

    public static int CURRENT_MUROTTAL_ITEM_POS;
    public static int CURRENT_AYAT_START;
    public static int CURRENT_AYAT_END;

    public static int CURRENT_AYAT;
    public static int CURRENT_SURAT;

    public static int CURRENT_LOOP = 1;
}