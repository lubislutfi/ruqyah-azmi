package CONST;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.InetAddress;

/**
 * Created by kopral on 10/9/16.
 */

public class FILE_CONST {

    public static int CURRENT_AYAT_DOWNLOAD;
    public static int CURRENT_SURAT_DOWNLOAD;

    public static String[] STR_QORI_LIST() {

        String[] arrQoriList = new String[QORI_LIST().length()];
        for (int i = 0; i < QORI_LIST().length(); i++) {
            try {
                arrQoriList[i] = QORI_LIST().getJSONObject(i).getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arrQoriList;
    }

    public static JSONArray QORI_LIST() {
        JSONArray value = null;

        String json_fileurl =
                "[" +
                        "{" +
                        "\"name\":\"Ayman Sowaid (Guru)\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Ayman_Sowaid_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Abdurrahman As-Sudais\"," +
                        "\"url\":\"http://www.everyayah.com/data/Abdurrahmaan_As-Sudais_192kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Akram Al-Alaqimy\"," +
                        "\"url\":\"http://www.everyayah.com/data/Akram_AlAlaqimy_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Mishary Rashid Alafasy\"," +
                        "\"url\":\"http://www.everyayah.com/data/Alafasy_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Khalid Al-Qahtani\"," +
                        "\"url\":\"http://www.everyayah.com/data/Khaalid_Abdullaah_al-Qahtaanee_192kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Muhammad Ayyub\"," +
                        "\"url\":\"http://www.everyayah.com/data/Muhammad_Ayyoub_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Nasser Al Qatami\"," +
                        "\"url\":\"http://www.everyayah.com/data/Nasser_Alqatami_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Shalah AbdulRahman Bukhatir\"," +
                        "\"url\":\"http://www.everyayah.com/data/Salaah_AbdulRahman_Bukhatir_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Saud Ash-Syuraim\"," +
                        "\"url\":\"http://www.everyayah.com/data/Saood_ash-Shuraym_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Maher Al Mueaqly\"," +
                        "\"url\":\"http://www.everyayah.com/data/Maher_AlMuaiqly_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Mahmoud Khalil Al-Hussary (Pelan)\"," +
                        "\"url\":\"http://www.everyayah.com/data/Husary_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Muhammad Abdul Karim\"," +
                        "\"url\":\"http://www.everyayah.com/data/Muhammad_AbdulKareem_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Abdul Muhsin al Qasim\"," +
                        "\"url\":\"http://www.everyayah.com/data/Muhsin_Al_Qasim_192kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Saad Al Ghamidi\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Ghamadi_40kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Fares Abbad\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Fares_Abbad_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Mahmoud Khalil Al-Hussary (Cepat)\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/husary_qasr_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Mahmoud Khalil Al-Hussary (Mujawwad)\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Husary_Mujawwad_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"AbdulBasit AbdulSamad\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Abdul_Basit_Murattal_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"AbdulBasit AbdulSamad (Mujawwad)\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/AbdulSamad_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"M Siddiq Al-Minshawi (Guru)\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Minshawy_Teacher_128kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"M Siddiq Al-Minshawi (Mujawwad)\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Minshawy_Mujawwad_64kbps\"" +
                        "}," +
                        "{" +
                        "\"name\":\"Hani Ar-Rifai\"," +
                        "\"url\":\"http://212.57.192.148/ayat/mp3/Hani_Rifai_192kbps\"" +
                        "}" +
                        "]";
        try {
            value = new JSONArray(json_fileurl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("everyayah.com");
            return !ipAddr.equals("");
        } catch (Exception e) {
            return false;
        }
    }

}
