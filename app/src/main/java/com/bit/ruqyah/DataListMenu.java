package com.bit.ruqyah;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by kopral on 7/22/17.
 */

public class DataListMenu {
    private String jsonPengertianDanHukum = "[{" +
            "'title':'Pengertian'," +
            "'subtitle':'Dalil definisi tentang ruqyah'," +
            "'icon':"+R.drawable.ic_book_black_24dp+
            "},{" +
            "'title':'Hukum'," +
            "'subtitle':'Dalil hukum-hukum Ruqyah'," +
            "'icon':"+R.drawable.ic_book_black_24dp+
            "}]";
    private String jsonTataCaraRuqyah = "[{" +
            "'title':'Sebelum Pengobatan'," +
            "'subtitle':'Menjelaskan  fase-fase sebelum pengobatan'," +
            "'icon':"+R.drawable.ic_book_black_24dp+
            "},{" +
            "'title':'Pengobatan'," +
            "'subtitle':'Menjelaskan  fase-fase saat pengobatan'," +
            "'icon':"+R.drawable.ic_book_black_24dp+
            "},{" +
            "'title':'Nasehat Untuk Pasien'," +
            "'subtitle':'Nasehat untuk pasien ketika menjalani pengobatan'," +
            "'icon':"+R.drawable.ic_book_black_24dp+
            "}]";
    private String jsonJinDanPenyebabGangguan = "[{" +
            "'title':'Mengenal Jin','subtitle':'Definisi tentang Jin'," +
            "'icon':"+R.drawable.ic_book_black_24dp+
            "},{" +
            "'title':'Gangguan Jin','subtitle':'Jenis gangguan Jin'," +
            "'icon':"+R.drawable.ic_book_black_24dp+
            "}]";
    private String jsonMurottal = "";
    /*
[
  {
    "name":"Pengertian dan Hukum"
  },{
    "name":"Tata Cara Ruqyah"
  },{
    "name":"Jin dan Penyebab Gangguan"
  },{
    "name":"Murottal"
  }
]

* */
    public JSONArray getMenuPengertianDanHukum() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(jsonPengertianDanHukum);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public JSONArray getMenuTataCaraRuqyah() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(jsonTataCaraRuqyah);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public JSONArray getMenuJinDanPenyebabGangguan() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(jsonJinDanPenyebabGangguan);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public JSONArray getMenuMurottal() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(jsonMurottal);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }


}