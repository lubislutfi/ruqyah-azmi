package com.bit.ruqyah;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class LatihanSoal extends AppCompatActivity {

    JSONArray jsonArray_soal;

    String str_soal;
    String str_pilihan_a;
    String str_pilihan_b;
    String str_pilihan_c;
    String str_pilihan_d;

    List<String> pilihan;

    TextView tv_soal;
    TextView tv_pilihan_ganda_a;
    TextView tv_pilihan_ganda_b;
    TextView tv_pilihan_ganda_c;
    TextView tv_pilihan_ganda_d;
    TextView tv_nomor_soal;

    int jml_salah;
    int jml_benar;
    int length_soal;
    float nilai;

    int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latihan_soal);

        tv_soal = (TextView) findViewById(R.id.tv_soal);
        tv_pilihan_ganda_a = (TextView) findViewById(R.id.tv_pilihan_ganda_a);
        tv_pilihan_ganda_b = (TextView) findViewById(R.id.tv_pilihan_ganda_b);
        tv_pilihan_ganda_c = (TextView) findViewById(R.id.tv_pilihan_ganda_c);
        tv_pilihan_ganda_d = (TextView) findViewById(R.id.tv_pilihan_ganda_d);
        tv_nomor_soal = (TextView) findViewById(R.id.tv_nomor_soal);

        jsonArray_soal = getSoal();
        pos=0;
        jml_salah = 0;
        jml_benar = 0;
        nilai = 0;
        gantiSoal();
    }

    public void gantiSoal(){
        try {
            pilihan = new ArrayList<String>();
            pilihan.add(jsonArray_soal.getJSONObject(pos).getString("benar"));
            pilihan.add(jsonArray_soal.getJSONObject(pos).getString("salah1"));
            pilihan.add(jsonArray_soal.getJSONObject(pos).getString("salah2"));
            pilihan.add(jsonArray_soal.getJSONObject(pos).getString("salah3"));
            Collections.shuffle(pilihan); //acak pilihan ganda

            //taruh variabel ke textview
            tv_soal.setText(jsonArray_soal.getJSONObject(pos).getString("pertanyaan"));
            tv_pilihan_ganda_a.setText(pilihan.get(0));
            tv_pilihan_ganda_b.setText(pilihan.get(1));
            tv_pilihan_ganda_c.setText(pilihan.get(2));
            tv_pilihan_ganda_d.setText(pilihan.get(3));
            tv_nomor_soal.setText(String.valueOf(pos+1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONArray getSoal() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(getString(R.string.json_soal));
            //Log.d("LatihanSoal","Soal Before Shuffle: "+jsonArray.toString());

            //ACAK SOAL
            // Implementing Fisher–Yates shuffle
            Random rnd = new Random();
            for (int i = jsonArray.length() - 1; i >= 0; i--)
            {
                int j = rnd.nextInt(i + 1);
                // Simple swap
                Object object = jsonArray.get(j);
                jsonArray.put(j, jsonArray.get(i));
                jsonArray.put(i, object);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public static int[] acakJawaban(int[] array){
        Random rgen = new Random();  // Random number generator

        for (int i=0; i<array.length; i++) {
            int randomPosition = rgen.nextInt(array.length);
            int temp = array[i];
            array[i] = array[randomPosition];
            array[randomPosition] = temp;
        }

        return array;
    }

    public void LatihanSoalClick(View v) {
        int id = v.getId();
        Intent i;
        switch (id) {
            case R.id.tv_pilihan_ganda_a:
                try {
                    TextView tv_jawab = (TextView) findViewById(id);
                    if(tv_jawab.getText().toString().equalsIgnoreCase(jsonArray_soal.getJSONObject(pos).getString("benar"))){
                        jml_benar++;
                    }else{
                        jml_salah++;
                    }
                    Log.d("LatihanSoal","jml_salah: "+jml_salah+"| jml_benar: "+jml_benar);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv_pilihan_ganda_b:
                try {
                    TextView tv_jawab = (TextView) findViewById(id);
                    if(tv_jawab.getText().toString().equalsIgnoreCase(jsonArray_soal.getJSONObject(pos).getString("benar"))){
                        jml_benar++;
                    }else{
                        jml_salah++;
                    }
                    Log.d("LatihanSoal","jml_salah: "+jml_salah+"| jml_benar: "+jml_benar);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv_pilihan_ganda_c:
                try {
                    TextView tv_jawab = (TextView) findViewById(id);
                    if(tv_jawab.getText().toString().equalsIgnoreCase(jsonArray_soal.getJSONObject(pos).getString("benar"))){
                        jml_benar++;
                    }else{
                        jml_salah++;
                    }
                    Log.d("LatihanSoal","jml_salah: "+jml_salah+"| jml_benar: "+jml_benar);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv_pilihan_ganda_d:
                try {
                    TextView tv_jawab = (TextView) findViewById(id);
                    if(tv_jawab.getText().toString().equalsIgnoreCase(jsonArray_soal.getJSONObject(pos).getString("benar"))){
                        jml_benar++;
                    }else{
                        jml_salah++;
                    }
                    Log.d("LatihanSoal","jml_salah: "+jml_salah+"| jml_benar: "+jml_benar);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

        if(pos<jsonArray_soal.length()-1){
            pos++;
            gantiSoal();
        }else{
            Log.d("LatihanSoal","length soal: "+jsonArray_soal.length());
            Log.d("LatihanSoal","length soal: "+jsonArray_soal.length());
            length_soal = jsonArray_soal.length();
            Log.d("LatihanSoal","jmlbenar soal: "+jml_benar);
            nilai = ((float)jml_benar/(float)jsonArray_soal.length())*100;
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setCancelable(false);
            alert.setTitle("Hasil Latihan");
            alert.setMessage("Jawaban Salah: "+(length_soal-jml_benar)+"\nJawaban Benar: "+jml_benar+"\nNilai: "+(int)nilai);

            alert.setPositiveButton("Lagi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    jml_salah = 0;
                    jml_benar = 0;
                    nilai = 0;
                    pos=0;
                    gantiSoal();
                }
            });

            alert.setNegativeButton("Kembali",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Intent i = new Intent(LatihanSoal.this, MainActivity.class);
                            startActivity(i);
                        }
                    });

            alert.show();
        }
    }

}
