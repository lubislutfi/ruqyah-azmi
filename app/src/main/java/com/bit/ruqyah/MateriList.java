package com.bit.ruqyah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class MateriList extends AppCompatActivity {

    DataListMenu dlm;
    String menuName;
    ListView lv_menu;
    JSONArray ja_menu;
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi_list);

        dlm = new DataListMenu();
        menuName = getIntent().getStringExtra("menuName");
        lv_menu = (ListView) findViewById(R.id.lv_menu);

        if (menuName.equalsIgnoreCase("pengertian_dan_hukum")) {
            ja_menu = dlm.getMenuPengertianDanHukum();
            Log.d("DataList", "Title: " + ja_menu.toString());
        } else if (menuName.equalsIgnoreCase("tata_cara_ruqyah")) {
            ja_menu = dlm.getMenuTataCaraRuqyah();
            Log.d("DataList", "Title: " + ja_menu.toString());
        } else if (menuName.equalsIgnoreCase("jin_dan_penyebab_gangguan")) {
            ja_menu = dlm.getMenuJinDanPenyebabGangguan();
            Log.d("DataList", "Title: " + ja_menu.toString());
        }

        //set adapter
        lv_menu.setAdapter(new AdapterListMenu(this, ja_menu));
        lv_menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selected = ((TextView) view.findViewById(R.id.tv_listview_title)).getText().toString();

                i = new Intent(MateriList.this, MateriDetail.class);
                i.putExtra("listClicked", selected);
                startActivity(i);
            }
        });

        Log.d("menuList", "menuName: " + menuName);
    }
}
