package com.bit.ruqyah;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import org.xml.sax.XMLReader;

public class MateriDetail extends AppCompatActivity {

    String contentTitle;
    TextView tv_materiDetail_content;
    String str_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi_detail);

        tv_materiDetail_content = (TextView) findViewById(R.id.tv_materiDetail_content);
        contentTitle = getIntent().getStringExtra("listClicked");
        Log.d("Materi Detail", "Content Title: " + contentTitle);

        if (contentTitle.equalsIgnoreCase("Pengertian")) {
            str_content = getString(R.string.definisi_ruqyah_content);
            Log.d("MateriDetail","Content String: "+str_content);
        } else if (contentTitle.equalsIgnoreCase("Hukum")) {
            str_content = getString(R.string.hukum_ruqyah_content);
            Log.d("MateriDetail","Content String: "+str_content);
        } else if (contentTitle.equalsIgnoreCase("Sebelum Pengobatan")) {
            str_content = getString(R.string.sebelum_pengobatan_content);
            Log.d("MateriDetail","Content String: "+str_content);
        } else if (contentTitle.equalsIgnoreCase("Pengobatan")) {
            str_content = getString(R.string.pengobatan_content);
            Log.d("MateriDetail","Content String: "+str_content);
        } else if (contentTitle.equalsIgnoreCase("Nasehat Untuk Pasien")) {
            str_content = getString(R.string.nasehat_untuk_pasien_content);
            Log.d("MateriDetail","Content String: "+str_content);
        } else if (contentTitle.equalsIgnoreCase("Mengenal Jin")) {
            str_content = getString(R.string.mengenal_jin);
            Log.d("MateriDetail","Content String: "+str_content);
        } else if (contentTitle.equalsIgnoreCase("Gangguan Jin")) {
            str_content = getString(R.string.gangguan_jin);
            Log.d("MateriDetail","Content String: "+str_content);
        }

        if (Build.VERSION.SDK_INT >= 24) {
            tv_materiDetail_content.setText(Html.fromHtml(str_content, Html.FROM_HTML_MODE_COMPACT,null,new UlTagHandler()));
        } else {
            tv_materiDetail_content.setText(Html.fromHtml(str_content,null,new UlTagHandler()));
        }

    }

    public class UlTagHandler implements Html.TagHandler{
        @Override
        public void handleTag(boolean opening, String tag, Editable output,
                              XMLReader xmlReader) {
            if(tag.equals("ul") && !opening) output.append("\n");
            if(tag.equals("li") && opening) output.append("\n\t•");
            if(tag.equals("br") && opening) output.append("\n");

        }
    }
}
