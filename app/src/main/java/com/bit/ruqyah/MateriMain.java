package com.bit.ruqyah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MateriMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi_main);
    }

    public void MateriMainClick(View v) {
        int id = v.getId();
        Intent i;
        switch (id) {
            case R.id.tombol_pengertian_dan_hukum:
                i = new Intent(MateriMain.this, MateriList.class);
                i.putExtra("menuName", "pengertian_dan_hukum");
                startActivity(i);
                break;
            case R.id.tombol_tata_cara_ruqyah:
                i = new Intent(MateriMain.this, MateriList.class);
                i.putExtra("menuName", "tata_cara_ruqyah");
                startActivity(i);
                break;
            case R.id.tombol_jin_dan_penyebab_gangguan:
                i = new Intent(MateriMain.this, MateriList.class);
                i.putExtra("menuName", "jin_dan_penyebab_gangguan");
                startActivity(i);
                break;
        }
    }

}
