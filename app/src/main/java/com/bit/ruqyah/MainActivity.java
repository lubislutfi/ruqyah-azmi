package com.bit.ruqyah;

import android.content.Intent;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    DataBaseHelper myDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //inisialisasi database quran
        //initialize db
        if (!initializeDB()) {
            Toast.makeText(this, "Ups, DB Bermasalah", Toast.LENGTH_SHORT).show();
        }
    }


    public void MainMenuClick(View v) {
        int id = v.getId();
        Intent i;
        switch (id) {
            case R.id.tombol_materi:
                i = new Intent(MainActivity.this, MateriMain.class);
                //i.putExtra("menuName", "pengertian_dan_hukum");
                startActivity(i);
                break;
            case R.id.tombol_murottal:
                i = new Intent(MainActivity.this, Murottal.class);
                i.putExtra("", "");
                startActivity(i);
                break;
            case R.id.tombol_latihan_soal:
                i = new Intent(MainActivity.this, LatihanSoal.class);
                //i.putExtra("", "");
                startActivity(i);
                break;
            case R.id.tombol_about:
                i = new Intent(MainActivity.this, About.class);
                //i.putExtra("", "");
                startActivity(i);
                break;
        }
    }

    private boolean initializeDB() {
        boolean success = true;
        myDbHelper = new DataBaseHelper(this);
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            Log.d("DBHELPER", "Unable to Create Database");
            //throw new Error("Unable to create database");
            success = false;
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            Log.d("DBHELPER", sqle.getMessage());
            success = false;
        }
        return success;
        // Log.d("DBHELPER","testDB: "+myDbHelper.getQuranText("2"));
    }
}