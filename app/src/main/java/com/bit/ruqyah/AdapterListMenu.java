package com.bit.ruqyah;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by kopral on 2/17/17.
 */

public class AdapterListMenu extends BaseAdapter {

    Context context;
    JSONArray labels;

    public AdapterListMenu(Context val_context, JSONArray val_labels){
        this.context =  val_context;
        this.labels = val_labels;
    }

    @Override
    public int getCount() {
        return labels.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listView;

        if (convertView == null) {

            listView = new View(context);

            // get layout from layout xml
            listView = inflater.inflate(R.layout.item_list_menu, null);

            // set value into textview & Imageview Icon
            TextView tv_listview_title = (TextView) listView
                    .findViewById(R.id.tv_listview_title);
            TextView tv_listview_subtitle = (TextView) listView
                    .findViewById(R.id.tv_listview_subtitle);
            ImageView imageView = (ImageView) listView
                    .findViewById(R.id.iv_listview_icon);

            try {
                tv_listview_title.setText(labels.getJSONObject(position).getString("title"));
                tv_listview_subtitle.setText(labels.getJSONObject(position).getString("subtitle"));
                imageView.setImageResource(labels.getJSONObject(position).getInt("icon"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            listView = (View) convertView;
        }
        return listView;
    }
}
