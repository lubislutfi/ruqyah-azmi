package com.bit.ruqyah;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import CONST.APPSTATE;
import CONST.FILE_CONST;
import CONST.PARAMS_CONST;
import CONST.PLAY_CONST;
import util.ConnectionUtil;
import util.FileUtility;
import util.NumberUtil;

public class Murottal extends AppCompatActivity implements AdapterQuran.ViewHolder.ClickListener {

    private static String PLAY = "PLAY";
    private static String PAUSE = "PAUSE";
    private static final int REQUEST_SAVINGFILE = 2;

    private NumberUtil nu;
    private FileUtility fu;
    private ConnectionUtil cu;
    DownloadFileAsync downloadAudio;

    private boolean playBasmalah = true;
    private String[] namaSurat;
    private String[] murottalList;
    private String[] murottalListItem;
    String[] jmlAyat;
    AssetFileDescriptor afdBasmalah;
    boolean firstIteration;
    private String fileUrl;
    RecyclerView rv_playing_quran;
    private ArrayList<HashMap<String, String>> arlist_quran;
    private AdapterQuran mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    DataBaseHelper db;
    LinearLayoutManager linLayManager_recview;

    String tampil;

    //media player
    MediaPlayer mp;
    MediaPlayer mpNext;

    TextView tv_play_heading;
    TextView tv_play_now_playing;

    private Runnable updateTimerThread;
    private long startTime;
    private Handler customHandler;

    long timeInMilliseconds;
    long timeSwapBuff;
    long updatedTime;

    ImageView iv_play_next;
    ImageView iv_play_prev;
    ImageView iv_play_play;
    ImageView iv_play_pause;

    ProgressDialog mProgressDialog;

    int currentPosPlaying;
    boolean isDownloaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_murottal);

        initialize();

    }

    private void initialize() {
        //internal value
        namaSurat = getResources().getStringArray(R.array.nama_surat);
        jmlAyat = getApplicationContext().getResources().getStringArray(R.array.jumlah_surat);
        arlist_quran = new ArrayList<HashMap<String, String>>();
        nu = new NumberUtil(this);
        fu = new FileUtility(this);
        db = new DataBaseHelper(this);
        cu = new ConnectionUtil(this);

        rv_playing_quran = (RecyclerView) findViewById(R.id.rv_play_quran);
        linLayManager_recview = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        iv_play_next = (ImageView) findViewById(R.id.iv_play_next);
        iv_play_prev = (ImageView) findViewById(R.id.iv_play_prev);
        iv_play_play = (ImageView) findViewById(R.id.iv_play_play);
        iv_play_pause = (ImageView) findViewById(R.id.iv_play_pause);
        tv_play_heading = (TextView) findViewById(R.id.tv_play_heading);
        tv_play_now_playing = (TextView) findViewById(R.id.tv_play_now_playing);

        //populate data to recyclerview
        murottalList = getResources().getStringArray(R.array.murrotal_list);
        PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS = 0;
        murottalListItem = murottalList[PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS].split(";");

        PLAY_CONST.CURRENT_SURAT = Integer.parseInt(murottalListItem[0]); //current surah
        PLAY_CONST.CURRENT_AYAT = Integer.parseInt(murottalListItem[1]); //start ayah
        PLAY_CONST.CURRENT_AYAT_START = Integer.parseInt(murottalListItem[1]); //start ayah
        PLAY_CONST.CURRENT_AYAT_END = Integer.parseInt(murottalListItem[2]); //end ayah
        currentPosPlaying = PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS; //keperluan oncompletion

        setRecyclerViewData(PLAY_CONST.CURRENT_SURAT, PLAY_CONST.CURRENT_AYAT_START, PLAY_CONST.CURRENT_AYAT_END);
    }

    private boolean setRecyclerViewData(int val_surah, int val_aya_start, int val_aya_end) {

        if (val_aya_start == val_aya_end) {
            tv_play_heading.setText(namaSurat[val_surah - 1] + ": " + val_aya_start);
        } else {
            tv_play_heading.setText(namaSurat[val_surah - 1] + ": " + val_aya_start + " - " + val_aya_end);
        }

        tv_play_now_playing.setText(namaSurat[val_surah - 1] + " " + val_aya_start);

        arlist_quran = db.getQuranText(String.valueOf(val_surah), String.valueOf(val_aya_start), String.valueOf(val_aya_end));
        rv_playing_quran.setHasFixedSize(true);
        //mRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        rv_playing_quran.setLayoutManager(linLayManager_recview);

        //int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        //rv_playing_quran.addItemDecoration(new DecorationFileManager(spacingInPixels));

        mAdapter = new AdapterQuran(Murottal.this, arlist_quran, this);
        rv_playing_quran.setAdapter(mAdapter);
        return true;
    }

    public void playMedia() {
        try {
            PLAY_CONST.IS_AUDIOQURAN_PAUSED = false;

            mp = new MediaPlayer();
            mp = MediaPlayer.create(Murottal.this, R.raw.basmalah);
            mp.start();

            //play timer
            //playPauseTimer();
            playBasmalah = true;

            changeCommandIcon(PLAY);
            createNextMediaPlayer();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PLAY", e.toString());
        }        //set pemutaran berikutnya
    }

    private void createNextMediaPlayer() {
        Log.d("ITERASI", "basmalah 1. " + String.valueOf(playBasmalah));
        Log.d("ITERASI", "xplay.currentsurat: " + PLAY_CONST.CURRENT_SURAT + " |play.currentayat: " + PLAY_CONST.CURRENT_AYAT);

        if (playBasmalah) {
            playBasmalah = false;

            //agar basmalah alfatihah gak dobel
            if( PLAY_CONST.CURRENT_SURAT ==1&& PLAY_CONST.CURRENT_AYAT==1 ){
                PLAY_CONST.CURRENT_AYAT=2;
            }
        } else {
            iterateNextParams();
            Log.d("ITERASI", "BASMALAH FALSE, NEXT");
        }

        Log.d("ITERASI", "basmalah 2. " + String.valueOf(playBasmalah));
        Log.d("ITERASI", "xplay2.currentsurat: " + PLAY_CONST.CURRENT_SURAT + " |play.currentayat: " + PLAY_CONST.CURRENT_AYAT);

        if (playBasmalah) {
            //afdBasmalah = getBaseContext().getResources().openRawResourceFd(R.raw.basmalah);
            //mpNext = MediaPlayer.create(mContext, mResId);
            mpNext = new MediaPlayer();
            mpNext = MediaPlayer.create(Murottal.this, R.raw.basmalah);
            mp.setNextMediaPlayer(mpNext);
            mp.setOnCompletionListener(onCompletionListener);
        } else {
            try {
                mpNext = new MediaPlayer();
                //set pemutaran berikutnya
                Log.d("ITERASI", "play.currentsurat: " + PLAY_CONST.CURRENT_SURAT + " |play.currentayat: " + PLAY_CONST.CURRENT_AYAT);
                fileUrl = fu.getParamQoriPath() + "/" + nu.zeroLeftPadding(3, PLAY_CONST.CURRENT_SURAT) + nu.zeroLeftPadding(3, PLAY_CONST.CURRENT_AYAT) + ".mp3";
                Log.d("MEDIAPLAYER", fileUrl);
                Log.d("ITERASI", "PLAY | SURAT: " + PLAY_CONST.CURRENT_SURAT + " | AYAT: " + PLAY_CONST.CURRENT_AYAT);
                mpNext.setDataSource(fileUrl);
                mpNext.prepare();
                mp.setNextMediaPlayer(mpNext);
                mp.setOnCompletionListener(onCompletionListener);

                // mp.reset();
                //mp.prepare();
                // mp.start();
                //tv_heading.setText(tampil);
                //tv_caption.setText(namaSurat[PLAY_CONST.CURRENT_SURAT - 1] + " " + String.valueOf(PLAY_CONST.CURRENT_AYAT));
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("PLAY QURAN", e.toString());
            }
        }
    }

    private void iterateNextParams() {
        //move to next file,
        if (PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS < murottalList.length) {
            Log.d("ITERASI", "ITERATENEXT.currentsurat: " + PLAY_CONST.CURRENT_SURAT + " |play.currentayat: " + PLAY_CONST.CURRENT_AYAT + " |play.currentayatEND: " + PLAY_CONST.CURRENT_AYAT_END);

            //setAya
            if (PLAY_CONST.CURRENT_AYAT < PLAY_CONST.CURRENT_AYAT_END) {
                PLAY_CONST.CURRENT_AYAT++;
            } else {
                if (PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS != murottalList.length - 1) {
                    PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS++;
                    PLAY_CONST.CURRENT_SURAT = Integer.parseInt(murottalList[PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS].split(";")[0]);
                    PLAY_CONST.CURRENT_AYAT = Integer.parseInt(murottalList[PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS].split(";")[1]);
                    PLAY_CONST.CURRENT_AYAT_END = Integer.parseInt(murottalList[PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS].split(";")[2]);
                } else {
                    //isStillDownloading = false;
                }

            }
        }
    }

    private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.release();
            mp = mpNext;

            /*tv_play_heading.setText(tampil);
            tv_play_now_playing.setText(namaSurat[PLAY_CONST.CURRENT_SURAT - 1] + " " + String.valueOf(PLAY_CONST.CURRENT_AYAT));
            //tv_playing_loop_current.setText(String.valueOf(PLAY_CONST.CURRENT_LOOP));
            Log.d("RECYCLERVIEW", "CURRENT SURAH: " + PLAY_CONST.CURRENT_SURAT);
*/

            Log.d("ITERASI", "ONCOMPLETION.currentsurat: " + PLAY_CONST.CURRENT_SURAT + " |ONCOMPLETION.currentayat: " + PLAY_CONST.CURRENT_AYAT);
            //populate data to recyclerview
            if (currentPosPlaying != PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS) {
                currentPosPlaying = PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS;
                nextMurottalList();
            } else {
               // Log.d("ITERASI", "ONCOMPLETION.SCROLLPOS_NORMAL: " + ((PLAY_CONST.CURRENT_AYAT) - 1) + " | ONCOMPLETION.STARTAYAH: " + PLAY_CONST.CURRENT_AYAT_START + " | ONCOMPLETION.SCROLLPOS_MODIFY: " + ((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1));
                linLayManager_recview.scrollToPositionWithOffset(((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1), 0);
                //linLayManager_recview.scrollToPositionWithOffset(1, 0);
            }

            //set current playing
            mAdapter.clearSelection();
            toggleSelection(((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1));

            createNextMediaPlayer();
            Log.d("MEDIAPLAYER", "NEXT");
        }
    };

    private void nextMurottalList() {
        Log.d("nextMurottalList", "NEXTMUROTTAL|"+"PLAY_CONST.CURRENT_SURAT: "+PLAY_CONST.CURRENT_SURAT+" | PLAY_CONST.CURRENT_AYAT: "+PLAY_CONST.CURRENT_AYAT);

       // playBasmalah = true;

        //populate quran to recyclerview
        //if not last murottal list, so next iteration
        murottalListItem = murottalList[PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS].split(";");
        PLAY_CONST.CURRENT_SURAT = Integer.parseInt(murottalListItem[0]); //current surah
        PLAY_CONST.CURRENT_AYAT = Integer.parseInt(murottalListItem[1]);
        PLAY_CONST.CURRENT_AYAT_START = Integer.parseInt(murottalListItem[1]); //start ayah
        PLAY_CONST.CURRENT_AYAT_END = Integer.parseInt(murottalListItem[2]); //end ayah

        if (setRecyclerViewData(PLAY_CONST.CURRENT_SURAT, PLAY_CONST.CURRENT_AYAT_START, PLAY_CONST.CURRENT_AYAT_END)) {
            linLayManager_recview.scrollToPositionWithOffset(((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1), 0);
        }
    }

    private void prevMurottalList() {

       //playBasmalah = true;

        //populate quran to recyclerview
        //if not last murottal list, so next iteration
        murottalListItem = murottalList[PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS].split(";");
        PLAY_CONST.CURRENT_SURAT = Integer.parseInt(murottalListItem[0]); //current surah
        PLAY_CONST.CURRENT_AYAT = Integer.parseInt(murottalListItem[1]);
        PLAY_CONST.CURRENT_AYAT_START = Integer.parseInt(murottalListItem[1]); //start ayah
        PLAY_CONST.CURRENT_AYAT_END = Integer.parseInt(murottalListItem[2]); //end ayah

        if (setRecyclerViewData(PLAY_CONST.CURRENT_SURAT, PLAY_CONST.CURRENT_AYAT_START, PLAY_CONST.CURRENT_AYAT_END)) {
            linLayManager_recview.scrollToPositionWithOffset(((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1), 0);
        }

    }

    private void requestWritePermission() {
        if (ContextCompat.checkSelfPermission(Murottal.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            //Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(Murottal.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                new AlertDialog.Builder(Murottal.this)
                        .setTitle("Hak Akses Penyimpanan")
                        .setMessage("Anda tidak mengijinkan penggunaan mikrofon pada ponsel. Untuk dapat menjalankan aplikasi, anda harus memberikan akses mikrofon")
                        .setPositiveButton("Buka Pengaturan", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Log.d("Permissions", "MBUH OPO IKI");
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(Murottal.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_SAVINGFILE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            startDownload();
        }

    }

    private void startDownload() {
        downloadAudio = new DownloadFileAsync();
        downloadAudio.execute();
        mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                downloadAudio.cancel(true);
                Log.d("DIALOG DOWNLOAD", "DISMISSED");
            }
        });

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadAudio.cancel(true);
                Log.d("DIALOG DOWNLOAD", "CANCELLED");
            }
        });

        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public void playActivityClick(View v) {
        switch (v.getId()) {
            case R.id.iv_play_prev:
                if (PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS > 0) {
                    PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS--;


                    prevMurottalList();
                    mAdapter.clearSelection();
                    toggleSelection(((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1));
                    if(mp != null || mpNext!=null)
                    {
                        mp.release();
                        mpNext.release();
                    }
                    playMedia();

                }
                break;
            case R.id.iv_play_next:
                if (PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS < murottalList.length - 1) {
                    PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS++;
                    nextMurottalList();

                    mAdapter.clearSelection();
                    toggleSelection(((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1));

                    if(mp != null || mpNext!=null)
                    {
                        mp.release();
                        mpNext.release();
                    }

                    playMedia();
                }
                break;
            case R.id.iv_play_pause:
                try {
                    if (mp.isPlaying()) {
                        mp.pause();
                        PLAY_CONST.IS_AUDIOQURAN_PAUSED = true;
                        changeCommandIcon(PAUSE);

                        //pause timer
                        //playPauseTimer();

                    }
                } catch (Exception e) {
                    Log.e("PLAY", e.toString());
                }
                break;
            case R.id.iv_play_play:
                if (isDownloaded == true) {
                    try {
                        mp.start();
                        PLAY_CONST.IS_AUDIOQURAN_PAUSED = false;
                        changeCommandIcon(PLAY);
                    } catch (Exception e) {
                        Log.e("PLAY", e.toString());
                    }
                } else {
                    requestWritePermission();
                }

                break;
        }
    }

    private void playPauseTimer() {
        if (PLAY_CONST.IS_AUDIOQURAN_PAUSED) {
            //pause timer
            timeSwapBuff += timeInMilliseconds;
            customHandler.removeCallbacks(updateTimerThread);
        } else {
            //playing timer
            startTime = SystemClock.uptimeMillis();
            customHandler.postDelayed(updateTimerThread, 0);
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {

        int murottalListPos = 0;
        int currentSurahDownload = Integer.parseInt(murottalList[murottalListPos].split(";")[0]);
        int currentAyahDownload = Integer.parseInt(murottalList[murottalListPos].split(";")[1]);
        int currentLastAyahDownload = Integer.parseInt(murottalList[murottalListPos].split(";")[2]);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Murottal.this);
            mProgressDialog.setMessage("Mengunduh audio");
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... aurl) {
            String downloadStatus = "SUCCESS";
            int count;

            //populate data to recyclerview
            // murottalList = getResources().getStringArray(R.array.murrotal_list);
            //murottalListItem = murottalList[PLAY_CONST.CURRENT_MUROTTAL_ITEM_POS].split(";");

            boolean isStillDownloading = true;

            //buat folder
            if (fu.checkCurrentQoriPath()) {
                Log.d("ITERASI_DL", "Qori Path Exist");
                do {
                    if (isCancelled()) {
                        break;
                    }

                    /*Log.d("ITERASI_DL", "------");
                    Log.d("ITERASI_DL", "currentsurat: " + FILE_CONST.CURRENT_SURAT_DOWNLOAD + " | currentayat: " + FILE_CONST.CURRENT_AYAT_DOWNLOAD);
                    Log.d("ITERASI_DL", "param.startsurat: " + PARAMS_CONST.PARAM_STARTSURAT + " |param.lastsurat: " + PARAMS_CONST.PARAM_LASTSURAT);
                    Log.d("ITERASI_DL", "param.startayat: " + PARAMS_CONST.PARAM_STARTAYAT + " | param.lastayat: " + PARAMS_CONST.PARAM_LASTAYAT);
                    Log.d("ITERASI_DL", "play.currentsurat: " + FILE_CONST.CURRENT_SURAT_DOWNLOAD + " |play.currentayat: " + FILE_CONST.CURRENT_AYAT_DOWNLOAD);
*/
                    //String fileName = nu.zeroLeftPadding(3, FILE_CONST.CURRENT_SURAT_DOWNLOAD) + "" + nu.zeroLeftPadding(3, FILE_CONST.CURRENT_AYAT_DOWNLOAD) + ".mp3";
                    String downloadPath = fu.getAudioFileUrl(currentSurahDownload, currentAyahDownload);
                    String localPath = fu.getAudioFileLocalPath(currentSurahDownload, currentAyahDownload);
                    Log.d("ITERASI_DL", "Download Iterasi, URL: " + downloadPath);
                    Log.d("ITERASI_DL", "Download Iterasi: LOCAL: " + localPath);

                    //cek apakah file ayat ada? kalau gak ada, download
                    if (fu.isAyatFileExist(localPath)) {
                        Log.d("ITERASI_DL", "Ayat File Exist: " + true);
                    } else {
                        Log.d("ITERASI_DL", "Ayat File Exist: " + false);
                        try {
                            if (cu.isNetworkAvailable()) {

                                URL url = new URL(downloadPath);
                                URLConnection conexion = url.openConnection();
                                conexion.connect();

                                int lenghtOfFile = conexion.getContentLength();
                                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);

                                File fileTemp = new File(fu.getMediaPath() + "/tempfile.mp3");

                                if (fileTemp.exists()) {
                                    fileTemp.delete();
                                }

                                InputStream input = new BufferedInputStream(url.openStream());
                                OutputStream output = new FileOutputStream(fileTemp);

                                // Log.d("DownloadFile", "URL: "+downloadPath);
                                // Log.d("DownloadFile", "Local Path: "+localPath);

                                byte data[] = new byte[1024];

                                long total = 0;
                                while ((count = input.read(data)) != -1) {
                                    total += count;
                                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                                    output.write(data, 0, count);
                                    //Log.d("WHILE DOWNLOAD", "" + count);
                                    if (isCancelled())
                                        break;
                                }

                                output.flush();
                                output.close();
                                input.close();

                                if (!isCancelled()) {
                                    File audioayah = new File(localPath);
                                    if (fileTemp.renameTo(audioayah)) {
                                        Log.d("ITERASI_DL", "file Temp Renamed");
                                    } else {
                                        Log.e("ITERASI_DL", "file Temp Can't Rename");
                                        break;
                                    }
                                }
                            } else {
                                downloadStatus = "NO";
                                Log.d("ITERASI_DL", "NO INTERNET");
                                break;
                            }
                        } catch (Exception e) {
                            Log.e("ITERASI_DL", e.toString());
                        }
                    }


                    //move to next file,
                    if (murottalListPos < murottalList.length) {
                        //setAya
                        if (currentAyahDownload < currentLastAyahDownload) {
                            currentAyahDownload++;
                        } else {
                            if (murottalListPos != murottalList.length - 1) {
                                murottalListPos++;
                                currentSurahDownload = Integer.parseInt(murottalList[murottalListPos].split(";")[0]);
                                currentAyahDownload = Integer.parseInt(murottalList[murottalListPos].split(";")[1]);
                                currentLastAyahDownload = Integer.parseInt(murottalList[murottalListPos].split(";")[2]);
                            } else {
                                isStillDownloading = false;
                            }

                        }
                    } else {
                        isStillDownloading = false;
                    }

                    if (isCancelled())
                        break;
                } while (isStillDownloading);

            } else {
                Log.e("ITERASI_DL", "Qori Path NOT Exist");
            }
            return downloadStatus;

        }

        protected void onProgressUpdate(String[] progress) {
            // Log.d("ANDRO_ASYNC", progress[0]);
            // Log.d("ANDRO_ASYNC", "Surat: " + FILE_CONST.CURRENT_SURAT_DOWNLOAD);
            // Log.d("ANDRO_ASYNC", "Ayat: " + FILE_CONST.CURRENT_AYAT_DOWNLOAD);
            mProgressDialog.setMessage("Mengunduh file audio...\nQS. " + currentSurahDownload + " " + namaSurat[currentSurahDownload - 1] + " Ayat " + currentAyahDownload);
            mProgressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String processStatus) {
            //playMedia();
            Log.d("ITERASI_DL", processStatus);
            mProgressDialog.dismiss();
            if (processStatus.equalsIgnoreCase("SUCCESS")) {
                APPSTATE.STATE = APPSTATE.PLAYING;
                //Intent itn = new Intent(Murottal.this, PlayActivity.class);
                //startActivity(itn);
                isDownloaded = true;

                mAdapter.clearSelection();
                toggleSelection(((PLAY_CONST.CURRENT_AYAT - (PLAY_CONST.CURRENT_AYAT_START - 1)) - 1));

                playMedia();

            } else {
                Toast.makeText(Murottal.this, "Ups, Gangguan koneksi untuk mengunduh file", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void changeCommandIcon(String val_state) {
        if (val_state.equalsIgnoreCase(PLAY)) {
            APPSTATE.STATE = APPSTATE.PLAYING;
            iv_play_play.setVisibility(View.GONE);
            iv_play_pause.setVisibility(View.VISIBLE);
        } else {
            APPSTATE.STATE = APPSTATE.PLAYING;
            iv_play_play.setVisibility(View.VISIBLE);
            iv_play_pause.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public boolean onItemLongClicked(int position) {
        return false;
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
    }
}
