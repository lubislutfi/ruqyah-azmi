package com.bit.ruqyah;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kopral on 3/18/17.
 */

public class AdapterQuran extends SelectableAdapterFIleManagement<AdapterQuran.ViewHolder> {

    private ArrayList<HashMap<String, String>> mArrayList;
    private Context mContext;
    private ViewHolder.ClickListener clickListener;
    private Typeface typefaceQuran;

    public AdapterQuran(Context context, ArrayList<HashMap<String, String>> arrayList, ViewHolder.ClickListener clickListener) {
        this.mArrayList = arrayList;
        this.mContext = context;
        this.clickListener = clickListener;
        typefaceQuran = Typeface.createFromAsset(context.getAssets(),"UthmanicHafs1Ver09.otf");
    }

    // Create new views
    @Override
    public AdapterQuran.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_quran, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView, clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.tv_row_playing_quran_text.setText(mArrayList.get(position).get("text"));
        viewHolder.tv_row_playing_ayah.setText(mArrayList.get(position).get("aya"));

        viewHolder.tv_row_playing_quran_text.setTypeface(typefaceQuran);
        viewHolder.tv_row_playing_ayah.setBackgroundResource(isSelected(position) ? R.drawable.circle_playing_selected : R.drawable.circle_playing);
        //viewHolder.selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView tv_row_playing_quran_text;
        public TextView tv_row_playing_ayah;

        private ClickListener listener;
        private final View selectedOverlay;

        public ViewHolder(View itemLayoutView, ClickListener listener) {
            super(itemLayoutView);
            this.listener = listener;

            tv_row_playing_quran_text = (TextView) itemLayoutView.findViewById(R.id.tv_row_playing_quran_text);
            tv_row_playing_ayah = (TextView) itemLayoutView.findViewById(R.id.tv_row_playing_ayah);
           // tvId = (TextView) itemLayoutView.findViewById(R.id.tvId);

            selectedOverlay = (View) itemView.findViewById(R.id.selected_overlay);

            itemLayoutView.setOnClickListener(this);
            itemLayoutView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                Log.d("RecViewListener", "onClick: " + getAdapterPosition());
                listener.onItemClicked(getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (listener != null) {
                Log.d("RecViewListener", "onLongClick: " + getAdapterPosition());
                return listener.onItemLongClicked(getAdapterPosition());
            }
            return false;
        }


        public interface ClickListener {
            public void onItemClicked(int position);
            public boolean onItemLongClicked(int position);
        }
    }
}
